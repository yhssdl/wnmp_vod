@ECHO OFF
echo 正在关闭 Nginx ......
plugin\Process -k nginx.exe >nul:
plugin\Process -k nginx.exe >nul:

if exist nginx\logs\nginx.pid del /Q /F nginx\logs\nginx.pid >nul:
echo 正在关闭 PHP ......
plugin\Process -k xxfpm.exe >nul:
plugin\Process -k php-cgi.exe >nul:

echo 正在关闭 MySQL ......
plugin\Process -k mysqld.exe >nul:

if exist mariadb\data\*.* del /Q /F mariadb\data\*.* >nul:
echo 正在关闭 Vod task ......
plugin\Process -k vod_task.exe >nul:

TIMEOUT /T 1
EXIT
