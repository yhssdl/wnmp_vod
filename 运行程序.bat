@ECHO OFF
plugin\Process -k nginx.exe >nul:
plugin\Process -k nginx.exe >nul:
if exist nginx\logs\nginx.pid del /Q /F nginx\logs\nginx.pid >nul:
plugin\Process -k xxfpm.exe >nul:
plugin\Process -k php-cgi.exe >nul:
plugin\Process -k redis-server.exe >nul:
plugin\Process -k mysqld.exe >nul:
plugin\Process -k vod_task.exe >nul:
if exist mariadb\data\%COMPUTERNAME%.pid del /Q /F mariadb\data\%COMPUTERNAME%.pid >nul:

set "HKLMU=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\"
reg query %HKLMU%{A94EC1B2-932B-49D7-8AF2-4FBD29FF314B}>nul 2>nul&&set VC15=Microsoft Visual C++ 2019 X64 Minimum Runtime - 14.23.27820
reg query %HKLMU%{7DC387B8-E6A2-480C-8EF9-A6E51AE81C19}>nul 2>nul&&set VC15=Microsoft Visual C++ 2019 X64 Minimum Runtime - 14.24.28127
reg query %HKLMU%{EEA66967-97E2-4561-A999-5C22E3CDE428}>nul 2>nul&&set VC15=Microsoft Visual C++ 2019 X64 Minimum Runtime - 14.25.28508
reg query %HKLMU%{37BB1766-C587-49AE-B2DB-618FBDEAB88C}>nul 2>nul&&set VC15=Microsoft Visual C++ 2019 X64 Minimum Runtime - 14.27.29112

if defined VC15 goto:startnginx
echo 正在安装 Microsoft Visual C++ 2015-2019 运行库 ......
plugin\VC_redist.x64.exe /q

:startnginx
echo 正在启动 Nginx ......
cd ./nginx
"..\plugin\RunHiddenConsole.exe" nginx.exe -c conf/nginx.conf
cd ..

:startmariadb
echo 正在启动 MySQL ......
plugin\RunHiddenConsole.exe  mariadb\bin\mysqld.exe
if exist mariadb\data\mysql\gtid_slave_pos.ibd goto startphp
echo 正在更新数据......
mariadb\bin\mysql.exe -uroot -pphpcj mysql < mariadb\mysql.sql

:startphp
echo 正在启动 PHP(FastCGI) ......
plugin\RunHiddenConsole.exe plugin\xxfpm.exe "php/php-cgi.exe -c php/php.ini" -n 5 -i 127.0.0.1 -p 9000

echo 正在启动 Vod Task......
plugin\RunHiddenConsole.exe plugin\vod_task.exe

for /f "tokens=16" %%i in ('ipconfig ^|find /i "ipv4"') do (
set ip=%%i
)
set url=http://%ip%/
start %url%/setup
start %url%
TIMEOUT /T 1
EXIT

